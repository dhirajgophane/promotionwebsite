package com.agiletestingalliance;

import static org.junit.Assert.*;

import org.junit.Test;

import com.agiletestingalliance.Duration;
import com.agiletestingalliance.MinMax;
import com.agiletestingalliance.AboutCPDOF;

public class PromotionSiteTest {
 
    @Test
    public void runDurationString() {
       Duration durationTest = new Duration();
       String results = durationTest.dur(); 
       assertEquals("CP-DOF is designed specifically for corporates and working professionals alike. If you are a corporate and can't dedicate full day for training, then you can opt for either half days course or  full days programs which is followed by theory and practical exams.", results); 

    }

    @Test
    public void runMinMax() {
       MinMax minmax = new MinMax();
       assertEquals(3, minmax.f(2,3));
       assertEquals(4, minmax.f(4,1));

}

@Test
    public void runAboutCPDOF() {
       AboutCPDOF about = new AboutCPDOF();
       String results = about.desc();
   
       assertTrue("label", results.contains("CP-DOF certification program covers end to end DevOps Life Cycle practically."));
 } 
}
